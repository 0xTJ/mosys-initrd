# Mosys OS Userspace

This is the userspace code for [Mosys](https://gitlab.com/0xTJ/mosys). It builds the C standard library and applications, and assembles `initrd.cpio`.

## Building

### With Mosys

The primary method of building is from the [mosys](https://gitlab.com/0xTJ/mosys) repository. Follow instructions there.

### Standalone

To build, use `cmake`, targetting the root of the repository, with the toolchain set to one in the `toolchains` directory (e.g. `mkdir build && cd build && cmake -DCMAKE_TOOLCHAIN_FILE=../toolchains/m68k-mosys.cmake .. && make`).

To increase build speed, pass the argument `-j<thread count>` to `make` (e.g. `make -j6`).
